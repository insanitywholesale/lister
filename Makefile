.PHONY: protos gorelease

protos:
	buf generate

gorelease:
	go install -v github.com/goreleaser/goreleaser@latest
	goreleaser --snapshot --skip=publish --clean
