# TODO

Fix protobuf/grpc types being passed to repo functions
Fix `.proto` definitions, need xRequest and xResponse messages and use google Empty type
Add openapiv2 UI, only swagger.json is served now
Add v2 API with indexed items
