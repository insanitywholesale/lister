package frontend

import (
	"context"
	"embed"
	"html/template"
	"io/fs"
	"log"
	"net/http"
	"os"
	"strings"

	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
)

var (
	//go:embed templates
	templates  embed.FS
	templatefs fs.FS
	lc         pb.ListerServiceClient
)

func init() {
	listerName := os.Getenv("LISTER_BACKEND_NAME")
	if listerName == "" {
		listerName = "localhost"
	}
	listerPort := os.Getenv("LISTER_BACKEND_PORT")
	if listerPort == "" {
		listerPort = "15200"
	}
	listerAddr := listerName + ":" + listerPort

	conn, err := grpc.NewClient(listerAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	lc = pb.NewListerServiceClient(conn)
	tfs, err := fs.Sub(templates, "templates")
	if err != nil {
		log.Fatal(err)
	}
	templatefs = tfs
}

func FormHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		w.Write([]byte("this a form submission endpoint, wtf u doin fam\n"))
		return
	}
	if r.Method == http.MethodPost {
		var title string
		var items []string
		var separator string
		formErr := r.ParseForm()
		if formErr != nil {
			http.Error(w, formErr.Error(), http.StatusBadRequest)
			return
		}
		var itemString string
		if (r.Form["title"] != nil) && (len(r.Form["title"][0]) > 0) {
			title = r.Form["title"][0]
		} else {
			http.Error(w, "Please set a title for the list", http.StatusBadRequest)
			return
		}
		if r.Form["separator"] != nil {
			separator = r.Form["separator"][0]
		} else {
			separator = " "
		}
		if r.Form["items"] != nil {
			itemString = r.Form["items"][0]
			items = strings.Split(itemString, separator)
		} else {
			http.Error(w, "Please add some items to the list", http.StatusBadRequest)
			return
		}
		log.Println(title, items)
		_, err := lc.CreateList(context.Background(), &pb.CreateListRequest{List: &pb.List{Title: title, Items: items}})
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		http.Redirect(w, r, "/ui", http.StatusMovedPermanently)
		return
	}
}

func ShowLists(w http.ResponseWriter, _ *http.Request) {
	l, err := lc.ListLists(context.Background(), &emptypb.Empty{})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	tfs, err := template.ParseFS(templatefs, "main.html", "list.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = tfs.ExecuteTemplate(w, "main", l)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func ShowForm(w http.ResponseWriter, _ *http.Request) {
	tfs, err := template.ParseFS(templatefs, "main.html", "form.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = tfs.ExecuteTemplate(w, "main", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
