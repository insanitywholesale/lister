module gitlab.com/insanitywholesale/lister

go 1.22.5

require (
	github.com/carlmjohnson/versioninfo v0.22.5
	github.com/gocql/gocql v1.6.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.21.0
	github.com/jackc/pgx/v4 v4.18.3
	github.com/rs/cors v1.11.0
	google.golang.org/genproto/googleapis/api v0.0.0-20240725223205-93522f1f2a9f
	google.golang.org/grpc v1.65.0
	google.golang.org/protobuf v1.34.2
)

require (
	github.com/golang/snappy v0.0.4 // indirect
	github.com/hailocab/go-hostpool v0.0.0-20160125115350-e80d13ce29ed // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgconn v1.14.3 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.3 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	github.com/jackc/pgtype v1.14.3 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	golang.org/x/crypto v0.25.0 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
	golang.org/x/text v0.16.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240725223205-93522f1f2a9f // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
)
