package rest

import (
	"context"
	"net/http"
	"strings"
	"time"

	"github.com/carlmjohnson/versioninfo"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/cors"
	front "gitlab.com/insanitywholesale/lister/frontend"
	gw "gitlab.com/insanitywholesale/lister/proto/lister/v1"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/encoding/protojson"
)

var openapiDocs []byte

func SaveVars(oaD []byte) {
	openapiDocs = oaD
}

func fallback(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("idk about that fam\n"))
}

func pong(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("pong\n"))
}

func getDocs(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		w.Write(openapiDocs)
		return
	}
}

func getInfo(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		versionInfo := "Version information about datayoinker:" +
			"\n\tVersion: " + versioninfo.Version +
			"\n\tRevision: " + versioninfo.Revision +
			"\n\tLastCommit: " + versioninfo.LastCommit.String() +
			"\n"
		w.Write([]byte(versionInfo))
	}
}

func RunGateway(grpcport string, restport string) error {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	gwmux := runtime.NewServeMux(
		// Add custom options
		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.HTTPBodyMarshaler{
			Marshaler: &runtime.JSONPb{
				MarshalOptions: protojson.MarshalOptions{
					UseProtoNames:   true,
					EmitUnpopulated: true,
				},
				UnmarshalOptions: protojson.UnmarshalOptions{
					DiscardUnknown: true,
				},
			},
		}),
	)

	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := gw.RegisterListerServiceHandlerFromEndpoint(ctx, gwmux, ":"+grpcport, opts)
	if err != nil {
		return err
	}

	handler := cors.Default().Handler(gwmux)

	gwServer := &http.Server{
		ReadTimeout:       5 * time.Second,
		WriteTimeout:      5 * time.Second,
		IdleTimeout:       30 * time.Second,
		ReadHeaderTimeout: 2 * time.Second,
		Addr:              ":" + restport,
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if strings.HasPrefix(r.URL.Path, "/api") {
				handler.ServeHTTP(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/docs") {
				getDocs(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/info") {
				getInfo(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/ping") {
				pong(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/ui") {
				front.ShowLists(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/form") {
				front.ShowForm(w, r)
				return
			}
			if strings.HasPrefix(r.URL.Path, "/submit") {
				front.FormHandler(w, r)
				return
			}
			fallback(w, r)
		}),
	}
	return gwServer.ListenAndServe()
}
