# build stage
FROM golang:1.22 AS build

ENV CGO_ENABLED=0
ENV GO111MODULE=on

WORKDIR /go/src/lister
COPY . .

RUN go get -v
RUN go vet -v
RUN go install -v

# run stage
FROM gcr.io/distroless/static-debian12:nonroot AS run

COPY --from=build /go/bin/lister /

EXPOSE 15200
EXPOSE 9392

CMD ["/lister"]
