package cassandra

import (
	"context"

	"github.com/gocql/gocql"
	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
)

type Repo struct {
	session *gocql.Session
}

var ctx = context.Background()

func newCassandraSession(hosts []string) (*gocql.Session, error) {
	cluster := gocql.NewCluster()
	cluster.Hosts = hosts
	cluster.Keyspace = "lister"
	cluster.Consistency = gocql.Quorum
	session, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}
	err = session.Query(createListTableQuery).WithContext(ctx).Exec()
	if err != nil {
		return nil, err
	}
	return session, nil
}

func NewCassandraRepo(hosts []string) (*Repo, error) {
	cassandraSession, err := newCassandraSession(hosts)
	if err != nil {
		return nil, err
	}
	repo := &Repo{
		session: cassandraSession,
	}
	return repo, nil
}

func (r *Repo) RetrieveAll() ([]*pb.List, error) {
	var id uint32
	var title string
	var items []string
	var lists []*pb.List

	scanner := r.session.Query(listRetrieveAllQuery).WithContext(ctx).Iter().Scanner()
	for scanner.Next() {
		err := scanner.Scan(&id, &title, &items)
		if err != nil {
			return nil, err
		}
		list := &pb.List{
			Id:    id,
			Title: title,
			Items: items,
		}
		lists = append(lists, list)
	}

	return lists, nil
}

func (r *Repo) Retrieve(id uint32) (*pb.List, error) {
	list := &pb.List{}

	err := r.session.Query(listRetrievalQuery, id).WithContext(ctx).Consistency(gocql.One).Scan(&list.Id, &list.Title, &list.Items)
	if err != nil {
		return nil, err
	}

	return list, nil
}

func (r *Repo) Save(list *pb.List) (*pb.List, error) {
	var id uint32
	err := r.session.Query(listMaxIDQuery).WithContext(ctx).Consistency(gocql.One).Scan(&id)
	if err != nil {
		return nil, err
	}
	id++
	err = r.session.Query(listInsertQuery, id, list.GetTitle(), list.GetItems()).WithContext(ctx).Exec()
	if err != nil {
		return nil, err
	}
	return r.Retrieve(id)
}

func (r *Repo) Remove(id uint32) error {
	err := r.session.Query(listDeleteQuery, id).WithContext(ctx).Exec()
	if err != nil {
		return err
	}
	return nil
}
