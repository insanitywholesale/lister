package postgres

import (
	"context"

	"github.com/jackc/pgx/v4"
	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
)

type Repo struct {
	client *pgx.Conn
	pgURL  string
}

var ctx = context.Background()

func newPostgresClient(url string) (*pgx.Conn, error) {
	client, err := pgx.Connect(ctx, url)
	if err != nil {
		return nil, err
	}
	err = client.Ping(ctx)
	if err != nil {
		return nil, err
	}
	_, err = client.Exec(ctx, createListTableQuery)
	if err != nil {
		return nil, err
	}
	return client, nil
}

func NewPostgresRepo(url string) (*Repo, error) {
	pgclient, err := newPostgresClient(url)
	if err != nil {
		return nil, err
	}
	repo := &Repo{
		pgURL:  url,
		client: pgclient,
	}
	return repo, nil
}

func (r *Repo) RetrieveAll() ([]*pb.List, error) {
	/*
		var listslice []*pb.List
		pgxscan.Select(ctx, r.client, &listslice, listRetrieveAllQuery)
		return &pb.Lists{Lists: listslice}, nil
	*/
	listslice := []*pb.List{}
	rows, err := r.client.Query(ctx, listRetrieveAllQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		list := &pb.List{}
		err = rows.Scan(
			&list.Id,
			&list.Title,
			&list.Items,
		)
		if err != nil {
			return nil, err
		}
		listslice = append(listslice, list)
	}
	return listslice, nil
}

func (r *Repo) Retrieve(id uint32) (*pb.List, error) {
	list := &pb.List{}
	row, err := r.client.Query(ctx, listRetrievalQuery, id)
	if err != nil {
		return nil, err
	}
	err = row.Scan(
		&list.Id,
		&list.Title,
		&list.Items,
	)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (r *Repo) Save(list *pb.List) (*pb.List, error) {
	var id uint32
	err := r.client.QueryRow(ctx, listInsertQuery,
		list.GetTitle(),
		list.GetItems(),
	).Scan(&id)
	if err != nil {
		return nil, err
	}
	list.Id = id
	return r.Retrieve(id)
}

func (r *Repo) Remove(id uint32) error {
	_, err := r.client.Query(ctx, listDeleteQuery, id)
	if err != nil {
		return err
	}
	return nil
}
