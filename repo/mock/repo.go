package mock

import (
	"errors"
	"strconv"

	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
)

var testlist = &pb.List{
	Id:    1,
	Title: "Future Optiplexes",
	Items: []string{
		"https://www.proshop.gr/index.php?_route_=Desktopsgr/Dell-Optiplex-9020-SFF-de0990-Dell-2",
	},
}

type ListRepo []*pb.List

var testlists ListRepo = []*pb.List{
	testlist,
	{
		Id:    2,
		Title: "Git forges",
		Items: []string{
			"https://gitlab.com/insanitywholesale",
			"https://github.com/insanitywholesale",
		},
	},
}

var listID uint32 = 3

func NewMockRepo() (ListRepo, error) {
	return testlists, nil
}

func (ListRepo) RetrieveAll() ([]*pb.List, error) {
	return testlists, nil
}

func (ListRepo) Retrieve(id uint32) (*pb.List, error) {
	for _, l := range testlists {
		if id == l.GetId() {
			return l, nil
		}
	}
	return nil, errors.New("no list with ID " + strconv.Itoa(int(id)) + " was found")
}

func (ListRepo) Save(list *pb.List) (*pb.List, error) {
	list.Id = listID
	listID++
	testlists = append(testlists, list)
	return list, nil
}

func (ListRepo) Remove(id uint32) error {
	i := id - 1
	testlists[len(testlists)-1], testlists[i] = testlists[i], testlists[len(testlists)-1]
	testlists = testlists[:len(testlists)-1]
	return nil
}
