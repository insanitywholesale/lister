package grpc

import (
	"context"
	"log"
	"os"
	"strings"

	models "gitlab.com/insanitywholesale/lister/models/v1"
	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
	"gitlab.com/insanitywholesale/lister/repo/cassandra"
	"gitlab.com/insanitywholesale/lister/repo/mock"
	"gitlab.com/insanitywholesale/lister/repo/postgres"
	"google.golang.org/protobuf/types/known/emptypb"
)

type Server struct {
	pb.UnimplementedListerServiceServer
}

var dbstore models.ListsRepo

func init() {
	cassURL := os.Getenv("CASS_URL")
	if cassURL != "" {
		if cassURL == "test" {
			db, err := cassandra.NewCassandraRepo([]string{"localhost:9042"})
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		} else {
			cassIPs := strings.Split(cassURL, ",")
			db, err := cassandra.NewCassandraRepo(cassIPs)
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		}
		return
	}
	pgURL := os.Getenv("PG_URL")
	if pgURL != "" {
		if pgURL == "test" {
			db, err := postgres.NewPostgresRepo("postgresql://tester:Apasswd@localhost:5432?sslmode=disable")
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		} else {
			db, err := postgres.NewPostgresRepo(pgURL)
			if err != nil {
				log.Fatalf("error %v", err)
			}
			dbstore = db
		}
		return
	}
	dbstore, _ = mock.NewMockRepo()
}

func (Server) ListLists(context.Context, *emptypb.Empty) (*pb.ListListsResponse, error) {
	lists, err := dbstore.RetrieveAll()
	if err != nil {
		return nil, err
	}
	return &pb.ListListsResponse{Lists: lists}, nil
}

func (Server) GetList(_ context.Context, req *pb.GetListRequest) (*pb.GetListResponse, error) {
	list, err := dbstore.Retrieve(req.GetId())
	if err != nil {
		return nil, err
	}
	return &pb.GetListResponse{List: list}, nil
}

func (Server) CreateList(_ context.Context, req *pb.CreateListRequest) (*pb.CreateListResponse, error) {
	list, err := dbstore.Save(req.GetList())
	if err != nil {
		return nil, err
	}
	return &pb.CreateListResponse{List: list}, nil
}

func (Server) DeleteList(_ context.Context, req *pb.DeleteListRequest) (*emptypb.Empty, error) {
	err := dbstore.Remove(req.GetId())
	if err != nil {
		return nil, err
	}
	return &emptypb.Empty{}, nil
}
