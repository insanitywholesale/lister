package models

import (
	pb "gitlab.com/insanitywholesale/lister/proto/lister/v1"
)

type ListsRepo interface {
	RetrieveAll() ([]*pb.List, error)
	Retrieve(id uint32) (*pb.List, error)
	Save(list *pb.List) (*pb.List, error)
	Remove(id uint32) error
}
